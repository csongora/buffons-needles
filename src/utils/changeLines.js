function changeLines(lineCount, canvasSize) {
    const x1 = -canvasSize/2;
    const x2 = canvasSize/2;
    let offs1Z1 = 0;
    let offs1Z2 = 0;
    let offs2Z1 = 0;
    let offs2Z2 = 0;
    const stepLength = canvasSize / (lineCount - 1);

    let lines = [];
    lines.push([[x1, 0.01, 0],[x2, 0.01, 0]]);
    for (let i = 1; i < lineCount / 2 + 1; i+=1) {
        if (offs1Z1 !== 0) {
            lines.push([[x1, 0.01, offs1Z1],[x2, 0.01, offs1Z2]]);
            lines.push([[x1, 0.01, offs2Z1],[x2, 0.01, offs2Z2]]);
        }

        offs1Z1 += stepLength;
        offs1Z2 += stepLength;
        offs2Z1 -= stepLength;
        offs2Z2 -= stepLength;
    }

    return lines;
}

export default changeLines;
