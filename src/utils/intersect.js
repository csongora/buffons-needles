function intersect(canvasSize, lineCount, y1, y2) {
    const stepSize = canvasSize / (lineCount - 1);
    return Math.floor(y1/stepSize) !== Math.floor(y2/stepSize) ? true : false;
}

export default intersect;
