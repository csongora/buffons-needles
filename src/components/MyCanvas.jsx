import React, { useState, useEffect } from "react";
import { Canvas } from '@react-three/fiber';
import { Physics, usePlane, useBox } from "@react-three/cannon";
import { OrbitControls, Line } from '@react-three/drei';
import changeLines from "../utils/changeLines";
import useParametersStore from "../store/parametersStore";
import { DoubleSide } from "three";
import intersect from "../utils/intersect";

function MyCanvas() {
  const lineCount = useParametersStore((state) => state.lineCount);
  const canvasSize = useParametersStore((state) => state.canvasSize);
  const lineLengthPercentage = useParametersStore((state) => state.lineLengthPercentage);
  const stickCount = useParametersStore((state) => state.stickCount);
  const linesToAdd = useParametersStore((state) => state.linesToAdd);
  const increaseHits = useParametersStore((state) => state.increaseHits);

  const [lines, setLines] = useState(changeLines(lineCount, canvasSize));
  const [shapesOnCanvas, setShapesOnCanvas] = useState([]);

  const simulate = useParametersStore((state) => state.simulate);
  const increaseStick = useParametersStore((state) => state.increaseStick);
  const reset = useParametersStore((state) => state.reset);

  useEffect(() => {
    setLines(changeLines(lineCount, canvasSize));
    setShapesOnCanvas([]);
    reset();
  }, [lineCount, canvasSize, lineLengthPercentage])

  useEffect(() => {
    if (stickCount !== 0){
      setShapesOnCanvas(
        [
          ...shapesOnCanvas, 
          <Stick key={shapesOnCanvas.length} />
        ]
      )
    }
  }, [stickCount])

  useEffect(() => {
    function stuff(i) {
      setTimeout(() => {
        increaseStick();
      }, 300 * i);
    }

    if(simulate !== 0){
      for (let i = 0; i < linesToAdd; i++) {
        stuff(i);
     }
    }
      
  }, [simulate])

  const MyPlane = () => {
    const [ref] = usePlane(() => ({
      mass: 1,
      rotation: [-Math.PI/2, 0, 0],
      type: "Static",
    }));
    return(
      <mesh
        ref={ref}
      >
        <planeGeometry args={[canvasSize, canvasSize]} />
        <meshStandardMaterial color="#b1d4fa" side={DoubleSide} />
      </mesh>
    );
  }

  const Stick = () => {
    const length = ((canvasSize / (lineCount - 1)) * lineLengthPercentage) / 100;
    const x = canvasSize*Math.random() - canvasSize/2;
    const y = 3*Math.random() + 1;
    const z = canvasSize*Math.random() - canvasSize/2;

    const radian = Math.PI * Math.random();
    const y1 = -.5 * length * Math.sin(radian) + z;
    const y2 = .5 * length * Math.sin(radian) + z;
    const hit = intersect(canvasSize, lineCount, y1, y2);

    if (hit) {
      increaseHits();
    }

    const [ref] = useBox(() => ({
      mass: 100,
      position: [x, y, z],
      args: [length, 0.05, 0.05],
      rotation: [0, radian, 0],
    }));
    return(
      <mesh ref={ref} dispose>
        <boxGeometry
          rotation={[0, radian, 0]}
          position={[x, y, z]}
          attach="geometry"
          args={[length, 0.05, 0.05]}
        ></boxGeometry>
        <meshStandardMaterial color={hit ? "green" : "red"} />
      </mesh>
    );
  }

  return(
    <Canvas
      camera={{ position: [0, 5, 0], fov: 90 }}
      shadows
    >
      <color attach="background" args={['white']} />
      <ambientLight intensity={1} />
      <Physics>
        {[...shapesOnCanvas]}
        <MyPlane />
        {lines.map((lineCoords) => (
          <Line
            key={lineCoords}
            points={lineCoords}
            color="black"
            lineWidth={1}
          />
        ))}
      </Physics>
      <OrbitControls />
    </ Canvas>
  );
}

export default MyCanvas;
