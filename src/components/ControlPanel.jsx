import React from "react";
import { Button, Card, Slider, Typography, Divider } from "@mui/material";

import useParametersStore from "../store/parametersStore";

function ControlPanel() {
    const lineCount = useParametersStore((state) => state.lineCount);
    const canvasSize = useParametersStore((state) => state.canvasSize);
    const lineLengthPercentage = useParametersStore((state) => state.lineLengthPercentage);
    const linesToAdd = useParametersStore((state) => state.linesToAdd);
    const setLineCount = useParametersStore((state) => state.setLineCount);
    const setCanvasSize = useParametersStore((state) => state.setCanvasSize);
    const setLineLengthPercentage = useParametersStore((state) => state.setLineLengthPercentage);
    const startSimulation = useParametersStore((state) => state.startSimulation);
    const setLinesToAdd = useParametersStore((state) => state.setLinesToAdd);
    const reset = useParametersStore((state) => state.reset);

    function valuetext(value) {
        return `${value}°C`;
    }

    return(
        <Card
            sx={{
                ml: 2,
                mr: -2,
                height: '100%',
                zIndex: 2,
                bgcolor: '#f5feff',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center'
            }}
            elevation={4}
        >
            <Divider />
            <Typography
                variant="h5"
                sx={{ alignSelf: 'center', mt: 3 }}
            >
                Line Count
            </Typography>
            <Slider
                aria-label="lineCount"
                defaultValue={lineCount}
                value={lineCount}
                getAriaValueText={valuetext}
                valueLabelDisplay="auto"
                onChange={(e) => setLineCount(e.target.value)}
                step={1}
                marks
                min={2}
                max={20}
                sx={{
                    color: '#eb5515',
                    mb: 3,
                    width: '80%',
                    mt: 5,
                    alignSelf: 'center'
                }}
            />
            <Divider />
            <Typography
                variant="h5"
                sx={{ alignSelf: 'center', mt: 3 }}
            >
                Canvas Size
            </Typography>
            <Slider
                aria-label="canvasSize"
                defaultValue={canvasSize}
                value={canvasSize}
                getAriaValueText={valuetext}
                valueLabelDisplay="auto"
                onChange={(e) => setCanvasSize(e.target.value)}
                step={1}
                marks
                min={2}
                max={20}
                sx={{
                    color: '#eb5515',
                    mb: 3,
                    width: '80%',
                    mt: 5,
                    alignSelf: 'center'
                }}
            />
            <Divider />
            <Typography
                variant="h5"
                sx={{ alignSelf: 'center', mt: 3 }}
            >
                Stick Size
            </Typography>
            <Slider
                aria-label="lineLengthPercentage"
                defaultValue={lineLengthPercentage}
                value={lineLengthPercentage}
                getAriaValueText={valuetext}
                valueLabelDisplay="auto"
                onChange={(e) => setLineLengthPercentage(e.target.value)}
                step={10}
                marks
                min={10}
                max={90}
                sx={{
                    color: '#eb5515',
                    mb: 3,
                    width: '80%',
                    mt: 5,
                    alignSelf: 'center'
                }}
            />
            <Divider />
            <Typography
                variant="h5"
                sx={{ alignSelf: 'center', mt: 3 }}
            >
                Number of Sticks
            </Typography>
            <Slider
                aria-label="linesToAdd"
                defaultValue={linesToAdd}
                value={linesToAdd}
                getAriaValueText={valuetext}
                valueLabelDisplay="auto"
                onChange={(e) => setLinesToAdd(e.target.value)}
                step={10}
                marks
                min={1}
                max={1000}
                sx={{
                    color: '#eb5515',
                    mb: 3,
                    width: '80%',
                    mt: 5,
                    alignSelf: 'center'
                }}
            />
            <Divider />
            <Button
                onClick={() => startSimulation()}
                variant="contained"
                sx={{
                    width: '90%',
                    mt: 5,
                    ml: 2
                }}
            >
                Add Sticks
            </Button>
            <Button
                onClick={() => reset()}
                variant="contained"
                sx={{
                    width: '90%',
                    mt: 5,
                    ml: 2
                }}
            >
                Reset
            </Button>
        </Card>
    );
}

export default ControlPanel;
