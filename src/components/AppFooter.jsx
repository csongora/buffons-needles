import { Box, Container } from '@mui/material';
import React from 'react';
import Copyright from './Copyright';

function AppFooter() {
  return (
    <Box
      component="footer"
      sx={{
        py: 3,
        px: 2,
        mt: 'auto',
        backgroundColor: (theme) => (theme.palette.primary),
      }}
    >
      <Container
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Copyright />
      </Container>
    </Box>
  );
}

export default AppFooter;
