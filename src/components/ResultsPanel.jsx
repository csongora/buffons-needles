import React from "react";
import { Card, Typography, Divider } from "@mui/material";
import useParametersStore from "../store/parametersStore";

function ResultsPanel() {
    const stickCount = useParametersStore((state) => state.stickCount);
    const hitsCount = useParametersStore((state) => state.hitsCount);
    const canvasSize = useParametersStore((state) => state.canvasSize);
    const lineCount = useParametersStore((state) => state.lineCount);
    const lineLengthPercentage = useParametersStore((state) => state.lineLengthPercentage);

    return(
        <Card
            sx={{
                bgcolor: '#f5feff',
                mr: 2,
                ml: -2, 
                height: '100%',
                zIndex: 2,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center'
            }}
            elevation={4}
        >
            <Divider />
            <Typography
                variant="h4"
                sx={{ alignSelf: 'center', mt: 6 }}
            >
                Total Sticks
            </Typography>
            <Typography
                variant="h3"
                color="green"
                sx={{ alignSelf: 'center', mb: 6 }}
            >
                {stickCount}
            </Typography>
            <Divider />
            <Typography
                variant="h4"
                sx={{ alignSelf: 'center', mt: 6 }}
            >
                Hits
            </Typography>
            <Typography
                variant="h3"
                color="orange"
                sx={{ alignSelf: 'center', mb: 6 }}
            >
                {hitsCount}
            </Typography>
            <Divider />
            <Typography
                variant="h4"
                sx={{ alignSelf: 'center', mt: 6 }}
            >
                Calculated PI
            </Typography>
            <Typography
                variant="h3"
                color="red"
                sx={{ alignSelf: 'center', mb: 6 }}
            >
                {stickCount !== 0 && ((2 * stickCount * ((canvasSize / (lineCount - 1)) * lineLengthPercentage / 100)) / (hitsCount * (canvasSize / (lineCount - 1)))).toFixed(4)}
            </Typography>
            <Divider />
        </Card>
    );
}

export default ResultsPanel;
