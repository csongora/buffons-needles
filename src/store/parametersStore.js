import create from 'zustand';

const useParametersStore = create((set) => ({
    canvasSize: 5,
    lineCount: 4,
    lineLengthPercentage: 50,
    stickCount: 0,
    hitsCount: 0,
    simulate: 0,
    linesToAdd: 1,

    setCanvasSize: (newSize) => set(() => ({
        canvasSize: newSize,
    })),
    
    setLineCount: (newCount) => set(() => ({
        lineCount: newCount,
    })),

    setLineLengthPercentage: (newSize) => set(() => ({
        lineLengthPercentage: newSize,
    })),

    increaseStick: () => set((state) => ({
        stickCount: state.stickCount + 1,
    })),

    increaseHits: () => set((state) => ({
        hitsCount: state.hitsCount + 1,
    })),

    startSimulation: () => set((state) => ({
        simulate: state.simulate + 1,
    })),

    setLinesToAdd: (newSize) => set(() => ({
        linesToAdd: newSize,
    })),

    reset: () => set(() => ({
        stickCount: 0,
        hitsCount: 0,
    })),
}))

export default useParametersStore;
